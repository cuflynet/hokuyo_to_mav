/****************************************************************************

Simulation of ultrasonic rangefinder

Nodes:
subscribed pose and quat from Vicon (geometry_msgs::TransformStamped)
published  pose and quat to MAVROS (geometry_msgs::PoseStamped)
both in 

****************************************************************************/

#include <ros/ros.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose.h>
#include <sensor_msgs/LaserScan.h>
#include <tf/transform_datatypes.h>
#include <tf/LinearMath/Matrix3x3.h>
#include <tf/LinearMath/Quaternion.h>
#include <gazebo_msgs/ModelStates.h>
#include <math.h>
#include <pthread.h>
#define PI 3.14159265

// p-thread calls
pthread_mutex_t topicBell;
pthread_cond_t newPose;
int gaz_count = 0;
int loop_hz = 100;
sensor_msgs::LaserScan laser_raw_msg;

int laser_received = 0;

void hokCallback(const sensor_msgs::LaserScan& msg)
{
  laser_raw_msg = msg;
  laser_received = 1;
}

int main(int argc, char **argv)
{ 
  //======= ROS Setup ================
  ros::init(argc, argv, "hokuyo_to_mav_node");
  ros::NodeHandle n; 
  //======= ROS Publishers ================
  ros::Publisher hok_to_mav_publisher = n.advertise<sensor_msgs::LaserScan>("obstacle_avoidance", 1);
  //======= ROS Subscribers ===============
  // Ros setup async spinners for subscibers
  ros::AsyncSpinner spinner(2);    
  // ROS setup subscriber
    ros::Subscriber hok_raw = n.subscribe("laser_raw",1,hokCallback);
  // Start the Spinner
  spinner.start();

   // Publisher Loop
  ros::Rate loop_rate(loop_hz);
  
  topicBell = PTHREAD_MUTEX_INITIALIZER;
  pthread_cond_init(&newPose, NULL);
  
  while (ros::ok())
  {
    sensor_msgs::LaserScan range_msg;
    range_msg.angle_min = 0;
    range_msg.angle_max = PI;
    range_msg.angle_increment = PI/2;
    range_msg.time_increment = 0.0;
    range_msg.scan_time = 1/10.0;
    range_msg.range_min = 0.0;
    range_msg.range_max = 6.0;
    range_msg.ranges.resize(3);
    range_msg.ranges = {0,0,0};
    range_msg.header.seq = gaz_count;
    range_msg.header.stamp = ros::Time::now();
    range_msg.header.frame_id = "laser";

    // Ultrasonic message is foward, right, back, left, down
    // Assuming zero yaw
    float front,right,left;
    if(laser_received){
    /*front = laser_raw_msg.ranges[255];
    right = laser_raw_msg.ranges[0];
    left = laser_raw_msg.ranges[511];*/
    int count = 0;
    double sum = 0;
    for(int i = 252;i <= 258; i++){
	if(laser_raw_msg.ranges[i] < 7.0){
		sum = sum + laser_raw_msg.ranges[i];
		count++;
	}
    }
    front = sum/(float)count;

    count = 0;
    sum = 0;
    for(int i = 0;i <= 6; i++){
	if(laser_raw_msg.ranges[i] < 7.0){
		sum = sum + laser_raw_msg.ranges[i];
		count++;
	}
    }
    right = sum/(float)count;

    count = 0;
    sum = 0;
    for(int i = 511;i >= 505; i--){
	if(laser_raw_msg.ranges[i] < 7.0){
		sum = sum + laser_raw_msg.ranges[i];
		count++;
	}
    }
    right = sum/(float)count;

    ROS_INFO("Front: %f",front);
    ROS_INFO("Right: %f",right);
    ROS_INFO("Left: %f\n",left);

    range_msg.ranges[0] = front;
    range_msg.ranges[1] = right;
    range_msg.ranges[2] = left;

    hok_to_mav_publisher.publish(range_msg);
    }
    // Spin Once:
    ros::spinOnce();
	// Maintain loop rate
    loop_rate.sleep();
  }
  return 0;
}




